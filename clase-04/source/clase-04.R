## Eduard Martinez
## update: 03-05-2023
rm(list=ls())

## **[0.] Configuración inicial**

#### **0.1 Instalar/llamar las librerías de la clase**
require(pacman) 
p_load(tidyverse,rio,skimr,viridis,
       gstat, #variogram
       sf, # Leer/escribir/manipular datos espaciales
       leaflet, # Visualizaciones dinámicas
       tmaptools, # geocode_OSM()
       nngeo, # st_nn function
       spdep, # Construct neighbours list from polygon list 
       osmdata) # Get OSM's data

#### **0.2 Importar conjuntos de datos**

## Inmuebles
houses <- import("input/house_prices.rds")
class(houses)

## dataframe to sf
houses <- st_as_sf(x = houses, ## datos
                   coords=c("lon","lat"), ## coordenadas
                   crs=4326) ## CRS
class(houses)

## Censo
mnz <- import("input/mgn_censo_2018.rds")

mnz

### **1.1. Filtrar datos**

## get chapinero
chapinero <- getbb(place_name = "UPZ Chapinero, Bogota", 
                   featuretype = "boundary:administrative", 
                   format_out = "sf_polygon") %>% .$multipolygon

## crop puntos con poligono
house_chapi <- st_intersection(x = houses , y = chapinero)

## **[2.] Recuperar información de las covariables**

## missing 
table(is.na(house_chapi$rooms))
house_chapi %>% lapply(function(x) table(is.na(x)))

## stringr
browseURL("https://evoldyn.gitlab.io/evomics-2018/ref-sheets/R_strings.pdf")

## example
word = "Hola mundo, hoy es 19 de julio de 2022"

## Detect Matches
str_detect(string = word , pattern = "19") ## Detect the presence of a pattern match

str_locate(string = word , pattern = "o") ##  Locate the positions of pattern matches in a string

str_count(string = word , pattern = "o") ## Count the number of matches in a string

## Subset Strings
str_extract(string = word , pattern = "19") ## Return the first pattern match found in each strin

str_match(string = word , pattern = "19") ## Return the first pattern match found in each string

str_sub(string = word , start = 1, end = 4)

str_sub(string = word , start = 1 , end = 10)

## Mutate Strings
str_replace(string = word , pattern = "19" , replacement = "10+9")

str_to_lower(string = word)

str_to_upper(string = word)

## Regular Expressions
str_replace_all(string = word , pattern = " " , replacement = "-")

str_replace_all(string = word , "[:blank:]" , replacement = "-")

str_replace_all(string = word , "19|2022" , replacement = "-")

str_replace_all(string = word , "[0-9]" , replacement = "-")

## aplicacion
house_chapi$description <- str_to_lower(house_chapi$description)

house_chapi$surface_total[49] ## not surface_total

house_chapi$surface_covered[49] ## not surface_covered

house_chapi$description[49] ## explore description

str_locate_all(string = house_chapi$description[49] , pattern = "136 mts") ## detect pattern

str_extract(string=house_chapi$description[49] , pattern= "136 mts") ## extrac pattern

x <- "[:space:]+[:digit:]+[:space:]+mts" ## pattern

str_locate_all(string = house_chapi$description[49] , pattern = x) ## detect pattern

str_extract(string=house_chapi$description[49] , pattern= x) ## extrac pattern

## make new var
house_chapi <- house_chapi %>% 
               mutate(new_surface = str_extract(string=description , pattern= x))
table(house_chapi$new_surface)

## another pattern
y = "[:space:]+[:digit:]+[:space:]+m2"
house_chapi = house_chapi %>% 
              mutate(new_surface = ifelse(is.na(new_surface)==T,
                                          str_extract(string=description , pattern= y),
                                          new_surface))
table(house_chapi$new_surface)

## **[3.] Dependencia espacial**

## motivacion
import("output/variograma.rds")

## sf to sp
house_chapi_sp <- house_chapi %>% as_Spatial()
house_chapi_sp

## estimations
variogram(price/1000000 ~ 1, house_chapi_sp , cloud = F , cressie=T) %>% plot()

house_chapi_sp$normal <- rnorm(n = nrow(house_chapi_sp),
                               mean = mean(house_chapi_sp$price/1000000),
                               sd = 1000)
  
db_plot = left_join(x = variogram(price/1000000 ~ 1, house_chapi_sp, cloud = F , cressie=T) %>% mutate(estimate=gamma) %>% select(dist,estimate),
                    y = variogram(normal ~ 1, house_chapi_sp, cloud = F , cressie=T) %>% mutate(normal=gamma) %>% select(dist,normal),"dist") 

db_plot %>% head()

plot = ggplot(db_plot) + 
geom_point(aes(x=dist, y=normal , fill="Datos aleatorios (Dist. Normal)"), shape=21, alpha=0.5, size=5 ) +
geom_point(aes(x=dist, y=estimate , fill="Precio de la vivienda (properati)"), shape=21, alpha=0.5, size=5 ) +
labs(caption = "Fuente: Properati", y = "Semivariograma", x = "Distancia de separación entre inmuebles", fill = "") + theme_bw()
plot
export(plot,"output/variograma.rds")

## **[4.] Vecinos espaciales**

### **4.1 vecinos en la manzana**
house_chapi = house_chapi %>%
              group_by(MANZ_CCNCT) %>%
              mutate(new_surface_2=median(surface_total,na.rm=T))

table(is.na(house_chapi$surface_total))

table(is.na(house_chapi$surface_total),
      is.na(house_chapi$new_surface_2)) # ahora solo tenemos menos missing values

### **4.2 Vecinos mas cercanos**

## definir submuestra
new_chapi <- house_chapi[st_buffer(house_chapi[100,],200),]

new_chapi

## obtener objeto sp
house_chapi_poly <- new_chapi %>% st_buffer(20) %>% as_Spatial() # poligonos

## obtener vecinos
nb_chapi = poly2nb(pl=house_chapi_poly , queen=T) # opcion reina

## vecinos del inmueble 29
nb_chapi[[29]]

leaflet() %>% addTiles() %>% 
addCircles(data=new_chapi[29,],col="red") %>% 
addCircles(data=new_chapi[nb_chapi[[29]],]) %>% 
addCircles(data=new_chapi[-nb_chapi[[29]],],col="green")

## hacer ejemplo para un vecino y comparar resultados
st_geometry(new_chapi) = NULL
vecinos = new_chapi[nb_chapi[[32]],]
yo = new_chapi[32,]

yo$surface_total
vecinos$surface_total
mean(vecinos$surface_total , na.rm=T)


